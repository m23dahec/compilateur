open Ast
open FunPfx.Ast
open BinOp
let op_to_Pfx = function
  |Badd -> Add
  |Bsub -> Sub
  |Bmul -> Mul
  |Bdiv -> Div
  |Bmod -> Rem

let rec generate env dpth = function
  | Const n -> [Push(n)], dpth +1
  | Binop(op,e1,e2) -> 
    let seq2, dpth2 = generate env dpth e2 in 
    let seq1, dpth1 = generate env dpth2 e1 in 
    seq2 @ seq1 @ [op_to_Pfx op], dpth1-1
  | Uminus e -> 
    let seq, dpth2zinzin = generate env dpth e in
    seq @ [Push(0); Sub], dpth2zinzin
  | App(e1,e2) -> 
    let seq2, dpth2 = generate env dpth e2 in 
    let seq1, dpth1 = generate env dpth2 e1 in 
    seq2 @ seq1 @ [Exec;Swap;Pop],dpth1 -1
  | Fun(x,e) -> 
      let seq,_ = generate ((x,dpth)::env) (dpth+1) e in
    [PushExec(seq)],dpth
  | Var v ->
      let pos = List.assoc v env in
    [Push(dpth-pos);Get], dpth+1

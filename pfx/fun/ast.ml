type command =
   Push of int
  |Pop
  |Swap
  |Sub
  |Add
  |Mul
  |Div
  |Rem
  |Exec
  |Get
  |PushExec of command list

type program = int * command list

(*add here all useful functions and types  related to the AST: for instance  string_of_ functions *)

let rec string_of_command = function
   Push(a)  -> "push " ^ string_of_int(a)
  |Pop-> "pop"
  |Swap-> "swap"
  |Sub-> "sub"
  |Add-> "add"
  |Mul-> "mul"
  |Div-> "div"
  |Rem-> "rem"
  |Exec -> "exec"
  |Get -> "get"
  |PushExec(cmmds)-> "(" ^ String.concat " " (List.map string_of_command cmmds) ^ ")"

let string_of_commands cmds = String.concat " " (List.map string_of_command cmds)

let string_of_program (args, cmds) = Printf.sprintf "%i args: %s\n" args (string_of_commands cmds);;


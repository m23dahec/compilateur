val op_to_Pfx : BinOp.t -> BasicPfx.Ast.command

(* Function that generate a Pfx program from an Expr program *)
val generate : Ast.expression -> BasicPfx.Ast.command list

open Ast
open BasicPfx.Ast
open BinOp
let op_to_Pfx = function
  |Badd -> Add
  |Bsub -> Sub
  |Bmul -> Mul
  |Bdiv -> Div
  |Bmod -> Rem

let rec generate = function
  | Const n -> [Push(n)]
  | Binop(op,e1,e2) -> generate e2 @ generate e1 @ [op_to_Pfx op]
  | Uminus e -> generate e @ [Push(0); Sub]
  | Var _ -> failwith "Not yet supported"

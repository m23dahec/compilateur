open Ast
open Printf

let string_of_stack stack = sprintf "[%s]" (String.concat ";" (List.map string_of_int stack))

let string_of_state (cmds,stack) =
  (match cmds with
   | [] -> "no command"
   | cmd::_ -> sprintf "executing %s" (string_of_command cmd))^
    (sprintf " with stack %s" (string_of_stack stack))

(* Question 4.2 implementation *)
let step state =
  match state with
  | [], _ -> Error("Nothing to step", state )
  | Push(n) :: q, stack -> Ok (q, n :: stack)
  | Pop :: q, _ :: s -> Ok (q, s)
  | Pop :: q, [] -> Error("Runtime error: Stack underflow with Pop", (q, []))
  | Swap :: q, n1 :: n2 :: s -> Ok (q, n2 :: n1 :: s)
  | Swap :: q, _ -> Error("Runtime error: Not enough elements for Swap", (q, []))
  | Add :: q, n1 :: n2 :: s -> Ok (q, (n2 + n1) :: s)
  | Add :: q, _ -> Error("Runtime error: Not enough elements for Add", (q, []))
  | Sub :: q, n1 :: n2 :: s -> Ok (q, (n1 - n2) :: s)
  | Sub :: q, _ -> Error("Runtime error: Not enough elements for Sub", (q, []))
  | Mul :: q, n1 :: n2 :: s -> Ok (q, (n2 * n1) :: s)
  | Mul :: q, _ -> Error("Runtime error: Not enough elements for Mul", (q, []))
  | Div :: q, n1 :: n2 :: s when n2 != 0 -> Ok (q, (n1 / n2) :: s)
  | Div :: q, _ :: 0 :: _ -> Error("Runtime error: Division by zero", (q, []))
  | Div :: q, _ -> Error("Runtime error: Not enough elements for Div", (q, []))
  | Rem :: q, n1 :: n2 :: s when n2 != 0 -> Ok (q, (n1 mod n2) :: s)
  | Rem :: q, _ :: 0 :: _ -> Error("Runtime error: Division by zero in Rem", (q, []))
  | Rem :: q, _ -> Error("Runtime error: Not enough elements for Rem", (q, []))


let eval_program (numargs, cmds) args =
  let rec execute = function
    | [], []    -> Ok None
    | [], v::_  -> Ok (Some v)
    | state ->
       begin
         match step state with
         | Ok s    -> execute s
         | Error e -> Error e
       end
  in
  if numargs = List.length args then
    match execute (cmds,args) with
    | Ok None -> printf "No result\n"
    | Ok(Some result) -> printf "= %i\n" result
    | Error(msg,s) -> printf "Raised error %s in state %s\n" msg (string_of_state s)
  else printf "Raised error \nMismatch between expected and actual number of args\n";;

%{
    open Ast
%}

(**************
 * The tokens *
 **************)

(* enter tokens here, they should begin with %token *)
%token EOF ADD MUL DIV MOD POP SWAP GET EXEC LPAR RPAR
%token <int> INT PUSH
(******************************
 * Entry points of the parser *
 ******************************)

(* enter your %start clause here *)
%start <Ast.program> program

%%

(*************
 * The rules *
 *************)

(* list all rules composing your grammar; obviously your entry point has to be present *)
program: args=INT cmds=commands EOF { (args, cmds) }

commands: { [] }
        | head=cmd cmds=commands { head :: cmds }

cmd: PUSH { Push($1) }
   | POP { Pop }
   | SWAP { Swap }
   | ADD { Add }
   | MUL { Mul }
   | DIV { Div }
   | MOD { Rem }
   | GET { Get }
   | EXEC { Exec }
   | LPAR cmmds=commands RPAR { PushExec(cmmds) }
%%

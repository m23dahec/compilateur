{
  open Utils
  open Parser

  let print_token = function
    | EOF -> print_string "EOF"
    | ADD -> print_string "ADD"
    | DIV -> print_string "DIV"
    | MOD -> print_string "MOD"
    | INT i -> print_int i 
    | MUL -> print_string "MUL"
    | PUSH i -> print_string ("PUSH "^string_of_int i) 
    | POP -> print_string "POP"
    | SWAP -> print_string "SWAP"

  let mk_int nb loc =
    try INT (int_of_string nb)
    with Failure _ -> raise (Location.Error(Printf.sprintf "Illegal integer '%s': " nb,loc))
}

  (* Regular expression definitions *)
  let newline = (['\n' '\r'] | "\r\n")
  let blank = [' ' '\014' '\t' '\012']
  let not_newline_char = [^ '\n' '\r']
  let digit = ['0'-'9']

rule token = parse
  | newline { Location.incr_line lexbuf; token lexbuf }
  | blank+ { token lexbuf }
  | eof { EOF }
  | "--" not_newline_char* newline? { token lexbuf }
  | digit+ as nb { mk_int nb (Location.curr lexbuf) }
  | "push "+(digit+ as nb) { PUSH(int_of_string nb) }
  | "pop" { POP }
  | "swap" { SWAP }
  | "add" { ADD }
  | "mul" { MUL }
  | "div" { DIV }
  | "mod" { MOD }
  | _ as c { raise (Location.Error(Printf.sprintf "Illegal character '%c': " c, Location.curr lexbuf)) }

